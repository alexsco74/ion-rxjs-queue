export interface QueueLogInterface {
  clickDate: Date,
  startDate: Date,
  endDate: Date
}

export class QueueLog implements QueueLogInterface {
  constructor(public clickDate: Date,
              public startDate: Date,
              public endDate: Date) {
  }
}
