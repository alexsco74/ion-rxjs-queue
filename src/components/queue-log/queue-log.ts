import { Component, Input } from '@angular/core';
import { QueueLogInterface } from '../../models/queue-log';

/**
 * Generated class for the QueueLogComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'queue-log',
  templateUrl: 'queue-log.html'
})
export class QueueLogComponent {

  text: string;
  @Input() log: QueueLogInterface;

  constructor() {
  }
}
