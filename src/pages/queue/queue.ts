import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable, Subscriber } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import * as QueryLogModel from '../../models/queue-log';
/**
 * Generated class for the QueuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-queue',
  templateUrl: 'queue.html'
})
export class QueuePage {

  queue: Observable<any>;
  queueSubscriber: Subscriber<any>;
  logs: QueryLogModel.QueueLogInterface[] = [];
  timeout: number = 1000;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.queue = new Observable((subscriber) => {
      this.queueSubscriber = subscriber;
    });
    this.queue.pipe(concatMap((clickDate) => {
      return this.alarm(clickDate, this.timeout);
    })).subscribe((log) => {
        this.logs.push(log);
      }
    );
  }

  alarm(clickDate, timeout): Promise<any> {
    let startDate = new Date();
    let beep = new Promise(
      resolve => {
        setTimeout(() => {
          let log = new QueryLogModel.QueueLog(clickDate, startDate, new Date());
          resolve(log);
        }, timeout);
      }
    );
    return beep;
  }

  addQueueItem() {
    this.queueSubscriber.next(new Date());
  }

  clearLogs() {
    this.logs = [];
  }
}
