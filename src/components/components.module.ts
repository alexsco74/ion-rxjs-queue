import { NgModule } from '@angular/core';
import { QueueLogComponent } from './queue-log/queue-log';
import { IonicPageModule } from 'ionic-angular';
@NgModule({
	declarations: [QueueLogComponent],
  imports: [
    IonicPageModule
  ],
	exports: [QueueLogComponent]
})
export class ComponentsModule {}
